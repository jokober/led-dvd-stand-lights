# LED DVD Stand Lights

This project contains code and instructions on how to build a fancy LED light out of your old CD or DVD stands that you no longer have any use for. Using a remote you can select different effects or change the brightness.

## Overview

## What do you need to make one
A
ESP8266 module with RX1 pin exposed. We used a WeMos D1 mini.

WS2812B LED strip (such as Adafruit Neopixels). These can be purchased for as little as $5-15 USD per meter.
5V power supply
3.3V-5V level shifter (optional, must be non-inverting)