/*
  This sketch introduces the use of a custom show() function.
  It borrows the DMA class from the NeoPixelBus library to use the ESP8266's DMA
  channel to drive LED updates instead of the default Adafruit_NeoPixel "bit bang"
  method.

  Keith Lord - 2018
  LICENSE
  The MIT License (MIT)
  Copyright (c) 2018  Keith Lord
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sub-license, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  CHANGELOG
  2018-05-30 initial version
*/

//LED
#include <WS2812FX.h>
#include <NeoPixelBus.h>

#define LED_PIN    3  // digital pin used to drive the LED strip, (for ESP8266 DMA, must use GPIO3/RX/D9)
#define LED_COUNT 32  // number of LEDs on the strip

WS2812FX ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

// create a NeoPixelBus instance
NeoPixelBus<NeoGrbFeature, NeoEsp8266Dma800KbpsMethod> strip(LED_COUNT);

uint8_t brightness = 200;
uint8_t effectNumber = 0;

//IR
#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>

// An IR detector/demodulator is connected to GPIO pin 14(D5 on a NodeMCU
// board).
// Note: GPIO 16 won't work on the ESP8266 as it does not have interrupts.
const uint16_t kRecvPin = D5;

IRrecv irrecv(kRecvPin);

decode_results results;

void setup() {
  //LED
  Serial.begin(115200);

  ws2812fx.init();

  // MUST run strip.Begin() after ws2812fx.init(), so GPIO3 is initalized properly
  strip.Begin();
  strip.Show();

  // set the custom show function
  ws2812fx.setCustomShow(myCustomShow);

  ws2812fx.setBrightness(brightness);
  const uint32_t colors[] = {RED, BLACK, BLACK};
  ws2812fx.setSegment(0, 0, LED_COUNT - 1, FX_MODE_COMET, colors, 2000, NO_OPTIONS);

  ws2812fx.start();

  //IR
  irrecv.enableIRIn();  // Start the receiver
  while (!Serial)  // Wait for the serial connection to be establised.
    delay(50);
  Serial.println();
  Serial.print("IRrecvDemo is now running and waiting for IR message on Pin ");
  Serial.println(kRecvPin);
}

void loop() {
  ws2812fx.service();


  if (irrecv.decode(&results)) {
    // print() & println() can't handle printing long longs. (uint64_t)
    serialPrintUint64(results.value, HEX);

    switch (results.value) {
      case 0xE0E0E01F : increaseBrightness(); break;
      case 0xE0E0D02F : decreaseBrightness(); break;
      case 0xE0E048B7 : nextEffect(); break;
      case 0xE0E008F7 : previousEffect(); break;
    }
    irrecv.resume();
    delay(100);
  }
}

void decreaseBrightness() {
  brightness = brightness - 10;
  ws2812fx.setBrightness(brightness);
  Serial.println("Reduce Brightness");
}

void increaseBrightness() {
  brightness = brightness + 10;
  ws2812fx.setBrightness(brightness);
  Serial.println("IncreaseBrightness");
}

void nextEffect() {
  effectNumber++;
  Serial.println(effectNumber);
  changeEffect();
}

void previousEffect() {
  effectNumber--;
  Serial.println(effectNumber);
  changeEffect();
}

void changeEffect() {
  switch (effectNumber) {
    case 0 : ws2812fx.setMode(FX_MODE_BREATH); break;
    case 2 : ws2812fx.setMode(FX_MODE_COLOR_WIPE); break;
    case 3 : ws2812fx.setMode(FX_MODE_COLOR_WIPE_INV); break;
    case 4 : ws2812fx.setMode(FX_MODE_COLOR_WIPE_REV); break;
    case 5 : ws2812fx.setMode(FX_MODE_COLOR_WIPE_REV_INV); break;
    case 6 : ws2812fx.setMode(FX_MODE_COLOR_WIPE_RANDOM); break;
    case 7 : ws2812fx.setMode(FX_MODE_RANDOM_COLOR); break;
    case 8 : ws2812fx.setMode(FX_MODE_SINGLE_DYNAMIC); break;
    case 9 : ws2812fx.setMode(FX_MODE_MULTI_DYNAMIC); break;
    case 10 : ws2812fx.setMode(FX_MODE_RAINBOW); break;
    case 11 : ws2812fx.setMode(FX_MODE_RAINBOW_CYCLE); break;
    case 12 : ws2812fx.setMode(FX_MODE_SCAN); break;
    case 13 : ws2812fx.setMode(FX_MODE_DUAL_SCAN); break;
    case 14 : ws2812fx.setMode(FX_MODE_FADE); break;
    case 15 : ws2812fx.setMode(FX_MODE_THEATER_CHASE); break;
    case 16 : ws2812fx.setMode(FX_MODE_THEATER_CHASE_RAINBOW); break;
    case 17 : ws2812fx.setMode(FX_MODE_RUNNING_LIGHTS); break;
    case 18 : ws2812fx.setMode(FX_MODE_TWINKLE); break;
    case 19 : ws2812fx.setMode(FX_MODE_TWINKLE_RANDOM); break;
    case 20 : ws2812fx.setMode(FX_MODE_TWINKLE_FADE); break;
    case 21 : ws2812fx.setMode(FX_MODE_TWINKLE_FADE_RANDOM); break;
    case 22 : ws2812fx.setMode(FX_MODE_SPARKLE); break;
    case 23 : ws2812fx.setMode(FX_MODE_FLASH_SPARKLE); break;
    case 24 : ws2812fx.setMode(FX_MODE_HYPER_SPARKLE); break;
    case 25 : ws2812fx.setMode(FX_MODE_STROBE); break;
    case 26 : ws2812fx.setMode(FX_MODE_STROBE_RAINBOW); break;
    case 27 : ws2812fx.setMode(FX_MODE_MULTI_STROBE); break;
    case 28 : ws2812fx.setMode(FX_MODE_BLINK_RAINBOW); break;
    case 29 : ws2812fx.setMode(FX_MODE_CHASE_WHITE); break;
    case 30 : ws2812fx.setMode(FX_MODE_CHASE_COLOR); break;
    case 31 : ws2812fx.setMode(FX_MODE_CHASE_RANDOM); break;
    case 32 : ws2812fx.setMode(FX_MODE_CHASE_RAINBOW); break;
    case 33 : ws2812fx.setMode(FX_MODE_CHASE_FLASH); break;
    case 34 : ws2812fx.setMode(FX_MODE_CHASE_FLASH_RANDOM); break;
    case 35 : ws2812fx.setMode(FX_MODE_CHASE_RAINBOW_WHITE); break;
    case 36 : ws2812fx.setMode(FX_MODE_CHASE_BLACKOUT); break;
    case 37 : ws2812fx.setMode(FX_MODE_CHASE_BLACKOUT_RAINBOW); break;
    case 38 : ws2812fx.setMode(FX_MODE_COLOR_SWEEP_RANDOM); break;
    case 39 : ws2812fx.setMode(FX_MODE_RUNNING_COLOR); break;
    case 40 : ws2812fx.setMode(FX_MODE_RUNNING_RED_BLUE); break;
    case 41 : ws2812fx.setMode(FX_MODE_RUNNING_RANDOM); break;
    case 42 : ws2812fx.setMode(FX_MODE_LARSON_SCANNER); break;
    case 43 : ws2812fx.setMode(FX_MODE_COMET); break;
    case 44 : ws2812fx.setMode(FX_MODE_FIREWORKS); break;
    case 45 : ws2812fx.setMode(FX_MODE_FIREWORKS_RANDOM); break;
    case 46 : ws2812fx.setMode(FX_MODE_MERRY_CHRISTMAS); break;
    case 47 : ws2812fx.setMode(FX_MODE_FIRE_FLICKER); break;
    case 48 : ws2812fx.setMode(FX_MODE_FIRE_FLICKER_SOFT); break;
    case 49 : ws2812fx.setMode(FX_MODE_FIRE_FLICKER_INTENSE); break;
    case 50 : ws2812fx.setMode(FX_MODE_CIRCUS_COMBUSTUS); break;
    case 51 : ws2812fx.setMode(FX_MODE_HALLOWEEN); break;
    case 52 : ws2812fx.setMode(FX_MODE_BICOLOR_CHASE); break;
    case 53 : ws2812fx.setMode(FX_MODE_TRICOLOR_CHASE); break;
    case 54 : ws2812fx.setMode(FX_MODE_ICU); break;
  }
}

void myCustomShow(void) {
  if (strip.CanShow()) {
    // copy the WS2812FX pixel data to the NeoPixelBus instance
    memcpy(strip.Pixels(), ws2812fx.getPixels(), strip.PixelsSize());
    strip.Dirty();
    strip.Show();
  }
}
